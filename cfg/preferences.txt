# preferences.txt
# -m is mute
#-m
# -t is keep track of playtime
-t
# -f is fullscreen
#-f
# -w 800 sets screen width to 800
-w 800
# -h 480 sets screen height to 480
-h 480
# -z sets the zoom level
-z 4

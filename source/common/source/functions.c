// functions.c
#include "functions.h"
#ifndef _definitions_h
#include "definitions.h"
#endif

// our quick complaint function
void complainAndExit(void)
{
	printf("Error: %s\n", SDL_GetError());
}
// end quick complaint


// getpixel function retrieves the data from a single pixel
Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
	int bpp = surface->format->BytesPerPixel;

	/* Here p is the address to the pixel we want to retrieve */
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch(bpp) {
	case 1:
		return *p;
		break;
	case 2:
		return *(Uint16 *)p;
		break;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;
		break;
	case 4:
		return *(Uint32 *)p;
		break;
	default:
		return 0;       /* shouldn't happen, but avoids warnings */
	}
}
// end getpixel


// putpixel function writes one pixel to a surface
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	int bpp = surface->format->BytesPerPixel;

	/* Here p is the address to the pixel we want to set */
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch(bpp) {
	case 1:
		*p = pixel;
		break;

	case 2:
		*(Uint16 *)p = pixel;
		break;

	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
		  p[0] = (pixel >> 16) & 0xff;
		  p[1] = (pixel >> 8) & 0xff;
		  p[2] = pixel & 0xff;
		} else {
		  p[0] = pixel & 0xff;
		  p[1] = (pixel >> 8) & 0xff;
		  p[2] = (pixel >> 16) & 0xff;
		}
		break;

	case 4:
		*(Uint32 *)p = pixel;
		break;
	}
}
// end putpixel


// the grow function will double the size of an image
SDL_Surface* grow(SDL_Surface *surface, int zoom)
{

	int x   = 0;
	int y   = 0;
	int sx  = 0;
	int sy  = 0;

	int scale = zoom;  // how much we are scaling

        // the default resolution is 200x120
	SDL_Rect src;
	src.w   = surface->w * zoom;
	src.h   = surface->h * zoom;

	Uint32 pixel;

	SDL_Surface *bigSurface = SDL_CreateRGBSurface(surface->flags,
						       src.w,
						       src.h,
						       surface->format->BitsPerPixel,
						       surface->format->Rmask,
						       surface->format->Gmask,
						       surface->format->Bmask,
						       surface->format->Amask);

	for (y=0; y < surface->h; y++) {
		for (x=0; x < surface->w; x++) {
			for (sy=0; sy < scale; ++sy) {
				for (sx=0; sx < scale; ++sx) {
					pixel = getpixel(surface, x, y);
					putpixel(bigSurface,
						 (x * scale) + sx,
						 (y * scale) + sy,
						 pixel);
				}
			}
		}
	}
	return bigSurface;
}
// end grow function
  
// get_prefs_text() will read our preferences.txt file into a char[]
int *get_prefs_text(int prefs_text[], struct game_settings LoP_settings)
{
	int i = 0;
	int o = 0;
	char newline = 10;
	int temp[1024];
        for (i = 0; i < 1024; i++) {
                temp[i] = 0;
        }
	FILE *fp;
	#ifdef __POCC__
		fp = fopen("cfg\\preferences.txt", "r");
	#endif
	#ifdef __linux__
		fp = fopen("cfg/preferences.txt", "r");
	#endif
	if (fp==NULL)
		printf("Could not open preferences.txt!\n");

	/*
	 * this reads the entire file into prefs_text[].
	 */
	for (i = 0; i < 1024; i++) {
		temp[i]=fgetc(fp);
	}
	for (o = 0, i = 0; o < 1023; o++, i++) {        // left room for a final \n
                // skip the comments indicated by a '#'
		if (temp[o] == '#') {
                        while (temp[o] != newline && o < 1023) {
                                o++;
                        }
		}
                //printf("%c", temp[o]);
                // check to see if there was a newline 
                // the character after a newline should ALWAYS be '-' or '#'
                if (temp[o] == '\n') {
                        if ((temp[o+1] != '-') && (temp[o+1] != '#')) {
                                o = 1024;
                        }
                }
                // copy to text array
                prefs_text[i] = temp[o];
	}
        prefs_text[i + 1] = '\n';
	fclose(fp);

	return 0;
}
// end get_prefs_text

// get_terrain_text() will read our terrain.txt file into a char[]
int *get_terrain_text(int tiles[])
{
	int i = 0;
	int o = 0;
	char newline = 10;
	int temp[8000];
	FILE *fp;
	#ifdef __POCC__
		fp = fopen("content\\terrain.txt", "r");
	#endif
	#ifdef __linux__
		fp = fopen("content/terrain.txt", "r");
	#endif
	if (fp==NULL)
		printf("Could not open terrain.txt!\n");

	/*
	 * this reads the entire file into tiles[].
	 */
	// 20x10x32=6400 (ten by twenty scene, thirty two scenes)
	for (i=0;i<8000;i++) {
		temp[i]=fgetc(fp);
	}

	// clear the two variables we will use
	i=0;
	o=0;
	while (temp[o] != '\0') {
		// skip the comments indicated by a '#'
		while (temp[o] == '#' || temp[o] == newline) {
			if (temp[o] == '#') {
				while (temp[o] != newline) {
					//printf("%c",temp[o]);
					o++;
				}
				//printf("\n");
			}
			if (temp[o] == newline) {
				o++;
			}
		}
		tiles[i] = temp[o];
		//printf("tiles[%d]=%c\n",i, tiles[i]);
		i++;
		o++;
	}

	fclose(fp);

	return tiles;
}
// end get_terrain_text


/* get_map will load just the map tile you want
 * for a larger array. */
int get_map(int scene_number, int terrainTXT[], int i)
{
        //printf("get_map function called);
	int o = 0;

	o=200*scene_number;

	// return the required character
	return terrainTXT[i+o];
}
// end get_map
//
// turn a character right
int turn_right(int *direction, int *velocity_x, int *velocity_y)
{
        if (*direction == NORTH) {
                *direction = EAST;
                *velocity_x = 1;
                *velocity_y = 0;
                return 0;
        }
        if (*direction == SOUTH) {
                *direction = WEST;
                *velocity_x = -1;
                *velocity_y = 0;
                return 0;
        }
        if (*direction == WEST) {
                *direction = NORTH;
                *velocity_x = 0;
                *velocity_y = -1;
                return 0;
        }
        if (*direction == EAST) {
                *direction = SOUTH;
                *velocity_x = 0;
                *velocity_y = 1;
                return 0;
        }
        return 0;
}

// turn a character left
int turn_left(int *direction, int *velocity_x, int *velocity_y)
{
        if (*direction == NORTH) {
                *direction = WEST;
                *velocity_x = -1;
                *velocity_y = 0;
                return 0;
        }
        if (*direction == SOUTH) {
                *direction = EAST;
                *velocity_x = 1;
                *velocity_y = 0;
                return 0;
        }
        if (*direction == WEST) {
                *direction = SOUTH;
                *velocity_x = 0;
                *velocity_y = 1;
                return 0;
        }
        if (*direction == EAST) {
                *direction = NORTH;
                *velocity_x = 0;
                *velocity_y = -1;
                return 0;
        }
        return 0;
}
// turn a character around
int turn_around(int *direction, int *velocity_x, int *velocity_y)
{
        if (*direction == NORTH) {
                *direction = SOUTH;
                *velocity_x = 0;
                *velocity_y = 1;
                return 0;
        }
        if (*direction == SOUTH) {
                *direction = NORTH;
                *velocity_x = 0;
                *velocity_y = -1;
                return 0;
        }
        if (*direction == WEST) {
                *direction = EAST;
                *velocity_x = 1;
                *velocity_y = 0;
                return 0;
        }
        if (*direction == EAST) {
                *direction = WEST;
                *velocity_x = -1;
                *velocity_y = 0;
                return 0;
        }
        return 0;
}

// wobble_frame function changes the keyframe in a wobble pattern: 0->1->0->2
int wobble_frame(int step, int frameSpeed)
{
	// step just holds how many miliseconds have passed since the last "frameSpeed delay.
	// We provide a keyframe to match
	// each division of time: 1-25 is keyframe0, 26-50 is keyframe1, etc.
	if (step <= (frameSpeed / 4)) return 0;
	if (step > (frameSpeed / 4) && step <= (frameSpeed / 2)) return 1;
	if (step > (frameSpeed / 2) && step <= ((frameSpeed / 4) * 3)) return 0;
	if (step > ((frameSpeed / 4) * 3) && step <= frameSpeed) return 2;

	// added this line for Windows since it figured there was a possibility of
	// getting here somehow.
	return 0;
}
// end wobble_frame

// loop_frame function changes the keyframe in a rotating pattern: 0->1->2->0
int loop_frame(int step, int frameSpeed)
{
        if(step <= frameSpeed / 3) return 0;
        if((step > frameSpeed / 3) && (step < ((frameSpeed/ 3) * 2))) return 1; 
        if(((step > (frameSpeed / 3) * 2)) && (step < frameSpeed)) return 2; 

        return 0;
}
// end loop_frame

// return the x and y coordinates for the tile's representative character
int get_tile_coords(char scene_char, short int *x, short int *y, short int *block, int zoom) {
        *block = 0;
        //printf("received %c\n", scene_char);
        switch(scene_char) {
        // grass
        case '1':
                (*x) = 0;
                (*y) = 0;
                break;
        // grass into sand LR
        case '2':
                (*x) = 10 * zoom;
                (*y) = 0;
                break;
        // sand into grass LR
        case '3':
                (*x) = 20 * zoom;
                (*y) = 0;
                break;
        case '4':
                (*x) = 30 * zoom;
                (*y) = 0;
                break;
        case '5':
                (*x) = 40 * zoom;
                (*y) = 0;
                break;
        case '6':
                (*x) = 50 * zoom;
                (*y) = 0;
                break;
        case '7':
                (*x) = 60 * zoom;
                (*y) = 0;
                break;
        case '8':
                (*x) = 70 * zoom;
                (*y) = 0;
                break;
        case '9':
                (*x) = 80 * zoom;
                (*y) = 0;
                break;
        case '0':
                (*x) = 90 * zoom;
                (*y) = 0;
                break;
        case '~':
                (*x) = 100 * zoom;
                (*y) = 0;
                break;
        case '!':
                (*x) = 110 * zoom;
                (*y) = 0;
                break;
        case '@':
                (*x) = 120 * zoom;
                (*y) = 0;
                break;
        // just sand
        case '$':   
                (*x) = 130 * zoom;
                (*y) = 0;
                break;
        case '%':
                (*x) = 140 * zoom;
                (*y) = 0;
                // dynamically add the collision
                *block = 1;
                break;
        // North rock wall on grass
        case '^':
                (*x) = 150 * zoom;
                (*y) = 0;
                *block = 1;
                break;
        case '&':
                (*x) = 160 * zoom;
                (*y) = 0;
                *block = 1;
                break;
        case '*':
                (*x) = 170 * zoom;
                (*y) = 0;
                *block = 1;
                break;
        case '(':
                (*x) = 180 * zoom;
                (*y) = 0;
                *block = 1;
                break;
        case ')':
                (*x) = 190 * zoom;
                (*y) = 0;
                *block = 1;
                break;
        case 'q':       // flowers
                (*x) = 0;
                (*y) = 10 * zoom;
                *block = 0;
                break;
        // water
        case 'w':
                (*x) = 10 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'e':
                (*x) = 20 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'r':
                (*x) = 30 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 't':
                (*x) = 40 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'y':
                (*x) = 50 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // top right corner (water)
        case 'u':
                (*x) = 60 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'i':
                (*x) = 70 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // top water
        case 'o':
                (*x) = 80 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'p':
                (*x) = 90 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'Q':
                (*x) = 100 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'W':
                (*x) = 110 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'E':
                (*x) = 120 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // water body
        case 'R':
                (*x) = 130 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // bottom left rock on water
        case 'T':
                (*x) = 140 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // bottom rock on water
        case 'Y':
                (*x) = 150 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // bottom right rock on water
        case 'U':
                (*x) = 160 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // top left rock on water
        case 'I':
                (*x) = 170 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // top rock on water
        case 'O':
                (*x) = 180 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        // top right rock on water
        case 'P':
                (*x) = 190 * zoom;
                (*y) = 10 * zoom;
                *block = 1;
                break;
        case 'a':
                (*x) = 0;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 's':
                (*x) = 10 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'd':
                (*x) = 20 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'f':
                (*x) = 30 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'g':
                (*x) = 40 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'h':
                (*x) = 50 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'j':
                (*x) = 60 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'k':
                (*x) = 70 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'l':
                (*x) = 80 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case ';':
                (*x) = 90 * zoom;
                (*y) = 20 * zoom;
                //*block = 1;
                break;
        case 'A':
                (*x) = 100 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        // vertical, top sand bottom corrupt
        case 'S':
                (*x) = 110 * zoom;
                (*y) = 20 * zoom;
                *block = 0;
                break;
        // right corner of corrupt N,W,S all sand
        case 'D':
                (*x) = 120 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case 'F':
                (*x) = 130 * zoom;
                (*y) = 20 * zoom;
                *block = 0;
                break;
        case 'G':
                (*x) = 140 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case 'H':
                (*x) = 150 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case 'J':
                (*x) = 160 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case 'K':
                (*x) = 170 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case 'L':
                (*x) = 180 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case ':':
                (*x) = 190 * zoom;
                (*y) = 20 * zoom;
                *block = 1;
                break;
        case 'z':
                (*x) = 0;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case 'x':
                (*x) = 10 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case 'c':
                (*x) = 20 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case 'v':
                (*x) = 30 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case 'b':
                (*x) = 40 * zoom;
                (*y) = 30 * zoom;
                //*block = 1;
                break;
        case 'n':
                (*x) = 50 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case 'm':
                (*x) = 60 * zoom;
                (*y) = 30 * zoom;
                //*block = 1;
                break;
        // Rock top 
        case ',':
                (*x) = 70 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case '.':
                (*x) = 80 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case '/':
                (*x) = 90 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        case 'Z':
                (*x) = 100 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock ??? corner
        case 'X':
                (*x) = 110 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock ??? corner
        case 'C':
                (*x) = 120 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock ??? corner
        case 'V':
                (*x) = 130 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock desert bottom left
        case 'B':
                (*x) = 140 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock desert bottom middle 
        case 'N':
                (*x) = 150 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock desert bottom right
        case 'M':
                (*x) = 160 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock desert top left
        case '<':
                (*x) = 170 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock desert top middle
        case '>':
                (*x) = 180 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // Rock desert top right
        case '?':
                (*x) = 190 * zoom;
                (*y) = 30 * zoom;
                *block = 1;
                break;
        // green tree w/ apples
        case '-':
                (*x) = 0;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // green tree 
        case '_':
                (*x) = 10 * zoom;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // dead tree in grass
        case '=':
                (*x) = 20 * zoom;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // desert tree
        case '+':
                (*x) = 30 * zoom;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // desert tree no apples
        case '[':
                (*x) = 40 * zoom;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // dead desert tree
        case '{':
                (*x) = 50 * zoom;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // tainted tree with poison apples
        case ']':
                (*x) = 60 * zoom;
                (*y) = 40 * zoom;
                *block = 2;
                break;
        // tainted tree
        case '}':
                (*x) = 70 * zoom;
                (*y) = 40 * zoom;
                *block = 2;
                break;
        // dead tainted tree
        case '\'':
                (*x) = 80 * zoom;
                (*y) = 40 * zoom;
                *block = 2;
                break;
        // dungeon wall
        case 34: // that's a double quotes decimal value
                (*x) = 90 * zoom;
                (*y) = 40 * zoom;
                *block = 1;
                break;
        // black dungeon floor
        case '`':
                (*x) = 190 * zoom;
                (*y) = 110 * zoom;
                break;
        // Count Van Fire dungeon floor
        case '|':
                (*x) = 150 * zoom;
                (*y) = 110 * zoom;
        default:
                break;
        }
        return 0;
}
// end get tile coords

// Is a collision detected if so modify hit and/or collision variables?
int detect_collision(short int first_character_number, 
                int *first_x, 
                int *first_y, 
                int *first_velocity_x,
                int *first_velocity_y,
                int *first_direction,
                int *first_bump_direction,
                int *first_hit,
                short int second_character_number,
                int second_x, 
                int second_y,
                int second_direction,
                int zoom)
{
        int first_left_boundry = zoom;
        int first_right_boundry = (TILESIZE * zoom) - (zoom + zoom);
        int first_top_boundry = zoom * 3;
        int first_bottom_boundry = (TILESIZE * zoom) - zoom;

        int second_left_boundry = zoom;
        int second_right_boundry = (TILESIZE * zoom) - (zoom + zoom);
        int second_top_boundry = zoom * 3;
        int second_bottom_boundry = (TILESIZE * zoom) - zoom;

        // was a collision detected?
        int collision = 0;

        // Full square collision
        if(second_character_number == WALL) {
                second_left_boundry = 0;
                second_right_boundry = TILESIZE * zoom;
                second_top_boundry = 0;
                second_bottom_boundry = TILESIZE * zoom;
        } 
        // Ed 
        if(first_character_number == ED) {
                first_left_boundry = 1 * zoom;
                first_right_boundry = (TILESIZE * zoom) - (2 * zoom);
                first_top_boundry = 3 * zoom;
                first_bottom_boundry = (TILESIZE * zoom) - zoom;
        } 
        if(second_character_number == SWORD) {
                second_left_boundry = 1 * zoom;
                second_right_boundry = (TILESIZE * zoom) - (2 * zoom);
                second_top_boundry = 3 * zoom;
                second_bottom_boundry = (TILESIZE * zoom) - zoom;
        } 
        // more than full collision (sword)
        if(first_character_number == SWORD) {
                first_left_boundry = 0 + zoom;
                first_right_boundry = (TILESIZE * zoom) + zoom;
                first_top_boundry = 0 + zoom;
                first_bottom_boundry = (TILESIZE * zoom) + zoom;
        } 
        if(second_character_number == SWORD) {
                second_left_boundry = 0 + zoom;
                second_right_boundry = (TILESIZE * zoom) + zoom;
                second_top_boundry = 0 + zoom;
                second_bottom_boundry = (TILESIZE * zoom) + zoom;
        } 
        // cut the right pixel off and top
        if (second_character_number >= 0 && second_character_number < WALL) {
                second_left_boundry = 0;
                second_right_boundry = (TILESIZE * zoom) - zoom;
                second_top_boundry = zoom;
                second_bottom_boundry = TILESIZE * zoom;
        }
        // craig collision
        if (first_character_number == CRAIG) {
                first_left_boundry = 0;
                first_right_boundry = (TILESIZE * 2) * zoom;
                first_top_boundry = zoom * 3;
                first_bottom_boundry = ((TILESIZE * 2) * zoom) + (zoom * 2);
        }
        if (second_character_number == CRAIG) {
                second_left_boundry = 0;
                second_right_boundry = (TILESIZE * 2) * zoom;
                second_top_boundry = zoom * 3;
                second_bottom_boundry = ((TILESIZE * 2) * zoom) + (zoom * 2);
        }
        // king troll collision
        if (first_character_number == KING_TROLL) {
                first_left_boundry = zoom;
                first_right_boundry = (TILESIZE * 2) * zoom;
                first_top_boundry = zoom * -1;
                first_bottom_boundry = ((TILESIZE * 2) * zoom) - (zoom);
        }
        if (second_character_number == KING_TROLL) {
                second_left_boundry = zoom;
                second_right_boundry = (TILESIZE * 2) * zoom;
                second_top_boundry = zoom * -1;
                second_bottom_boundry = ((TILESIZE * 2) * zoom) - (zoom);
        }
        // count vanfire collision
        if (first_character_number == VANFIRE) {
                first_left_boundry = zoom * 2;
                first_right_boundry = (TILESIZE * 2) * zoom;
                first_top_boundry = zoom * -2;
                first_bottom_boundry = ((TILESIZE * 2) * zoom);
        }
        if (second_character_number == VANFIRE) {
                second_left_boundry = zoom * 2;
                second_right_boundry = (TILESIZE * 2) * zoom;
                second_top_boundry = zoom * -2;
                second_bottom_boundry = ((TILESIZE * 2) * zoom);
        }
        // Tempus collision
        if (first_character_number == TEMPUS) {
                first_left_boundry = zoom;
                first_right_boundry = (TILESIZE * 2) * zoom;
                first_top_boundry = zoom * 4;
                first_bottom_boundry = ((TILESIZE * 2) * zoom) - (zoom);
        }
        if (second_character_number == TEMPUS) {
                second_left_boundry = zoom;
                second_right_boundry = (TILESIZE * 2) * zoom;
                second_top_boundry = zoom * 4;
                second_bottom_boundry = ((TILESIZE * 2) * zoom) - (zoom);
        }
        // 0 = Ed, 1 = rat, 2 = troll, 3 = bat, 4 = ghost, 5 = rock, 6 = flame, 7 = ball, 8 = wall, 
        // 9 = sword, 10 = King Troll, 11 = VanFire, 12 = Tempus, 13 = Craig, 14-17 = windows, 18 = boundry
        switch(first_character_number) {
        case ED:
                // top left
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed top left collision detected!\n");
                        collision = TRUE;
                        // if second char is a first, then do damage
                        if (second_character_number > ED && 
                            second_character_number < WALL ||
                            second_character_number == KING_TROLL ||
                            second_character_number == VANFIRE) {
                                *first_hit = TRUE;
                        }
                        break;
                } 
                // top right
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed top right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        if (second_character_number > ED && 
                            second_character_number < WALL ||
                            second_character_number == KING_TROLL ||
                            second_character_number == VANFIRE) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom left
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        if (second_character_number > ED && 
                            second_character_number < WALL ||
                            second_character_number == KING_TROLL ||
                            second_character_number == VANFIRE) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom right
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        if (second_character_number > ED && 
                            second_character_number < WALL ||
                            second_character_number == KING_TROLL ||
                            second_character_number == VANFIRE) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                break;
        case 1:         // rat
                //if (first_character_number == 1) printf("rat\n");
        case 2:         // troll
                //if (first_character_number == 2) printf("troll\n");
        case 3:         // bat
                //if (first_character_number == 3) printf("bat\n");
        case 4:         // ghost
                //if (first_character_number == 4) printf("ghost\n");
        case 5:         // rock
                //if (first_character_number == 5) printf("rock\n");
        case 6:         // flame
                //if (first_character_number == 6) printf("flame\n");
        case 7:         // ball
                //if (first_character_number == 7) printf("ball\n");
                // top left
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Top left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if ghost go through walls
                        if (first_character_number == GHOST &&
                            second_character_number == WALL) {
                                collision = FALSE;
                        }
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                                *first_bump_direction = second_direction;
                                //printf("bump direction is: %i\n", *first_bump_direction);
                        }
                        if (first_character_number == ROCK) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                } 
                // top right
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed top right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if ghost go through walls
                        if (first_character_number == GHOST &&
                            second_character_number == WALL) {
                                collision = FALSE;
                        }
                        if (second_character_number == SWORD) {
                                *first_bump_direction = second_direction;
                                //printf("bump direction is: %i\n", *first_bump_direction);
                                *first_hit = TRUE;
                        }
                        if (first_character_number == ROCK) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // bottom left
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if ghost go through walls
                        if (first_character_number == GHOST &&
                            second_character_number == WALL) {
                                collision = FALSE;
                        }
                        if (second_character_number == SWORD) {
                                *first_bump_direction = second_direction;
                                //printf("bump direction is: %i\n", *first_bump_direction);
                                *first_hit = TRUE;
                        }
                        if (first_character_number == ROCK) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // bottom right
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Ed bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if ghost go through walls
                        if (first_character_number == GHOST &&
                            second_character_number == WALL) {
                                collision = FALSE;
                        }
                        if (second_character_number == SWORD) {
                                *first_bump_direction = second_direction;
                                //printf("bump direction is: %i\n", *first_bump_direction);
                                *first_hit = TRUE;
                        }
                        if (first_character_number == ROCK) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // boundry test
                //printf("checking boundries\n");
                if (*first_x > 190 * zoom) {       // screen is X: 0-200 Y: 20-120
                        *first_x = 190 * zoom;
                        //printf("first hit east boundry!\n");
                        collision = TRUE;
                }
                if (*first_x < 0) {
                        *first_x = 0;
                        //printf("first hit west boundry!\n");
                        collision = TRUE;
                }
                if (*first_y > 110 * zoom) {
                        *first_y = 110 * zoom;
                        //printf("first hit south boundry!\n");
                        collision = TRUE;
                }
                if (*first_y < 20 * zoom) {
                        *first_y = 20 * zoom;
                        //printf("first hit north boundry!\n");
                        collision = TRUE;
                }
                break;
        case 8:         // wall
                //if (first_character_number == 8) printf("wall\n");
                break;
        case KING_TROLL ... 13:
                //printf ("detecting collision for boss\n");
                //printf("First character number: %i\n", first_character_number);
                //printf("Second character number: %i\n", second_character_number);
                /*
                printf("boss:\tx(%i,%i)\n\ty(%i,%i)\n", 
                                *first_x + first_left_boundry, 
                                *first_x + first_right_boundry,
                                *first_y + first_top_boundry,
                                *first_y + first_bottom_boundry);
                printf("sword:\tx(%i,%i)\n\ty(%i,%i)\n\n", 
                                second_x + second_left_boundry, 
                                second_x + second_right_boundry,
                                second_y + second_top_boundry,
                                second_y + second_bottom_boundry);
                */
                // top left
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss top left collision detected!\n");
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                } 
                // top middle
                if (*first_x + (TILESIZE * zoom) + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + (TILESIZE * zoom) + first_left_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss top middle collision detected!\n");
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                } 
                // top right
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry &&
                    *first_y + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss top right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // right middle
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry &&
                    *first_y + (TILESIZE * zoom) + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + (TILESIZE * zoom) + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss top right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // bottom right
                if (*first_x + first_right_boundry >= second_x + second_left_boundry &&
                    *first_x + first_right_boundry <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // bottom middle
                if ((*first_x) + (TILESIZE * zoom) >= second_x + second_left_boundry &&
                    (*first_x) + (TILESIZE * zoom) <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // bottom left
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry && 
                    *first_y + first_bottom_boundry >= second_y + second_top_boundry &&
                    *first_y + first_bottom_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                }
                // left middle
                if (*first_x + first_left_boundry >= second_x + second_left_boundry &&
                    *first_x + first_left_boundry <= second_x + second_right_boundry &&
                    *first_y + (TILESIZE * zoom) + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + (TILESIZE * zoom) + first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss top left collision detected!\n");
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                } 
                // center 
                if (*first_x + (TILESIZE * zoom) + zoom >= second_x + second_left_boundry &&
                    *first_x + (TILESIZE * zoom) - zoom <= second_x + second_right_boundry &&
                    *first_y + (TILESIZE * zoom) + first_top_boundry >= second_y + second_top_boundry &&
                    *first_y + (TILESIZE * zoom) - first_top_boundry <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Boss center collision detected!\n");
                        collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD && first_character_number != TEMPUS) {
                                *first_hit = TRUE;
                                collision = FALSE;
                        }
                        break;
                } 
                break;
        case WINDOW1:
                // bottom right
                if (zoom * (TILESIZE * 5) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 5) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window1 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom middle
                if (zoom * (TILESIZE * 4) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 4) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window1 bottom middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom left
                if (zoom * (TILESIZE * 4) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 4) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window1 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top right (had to +5 to window top border to work with new sword collision)
                if (zoom * (TILESIZE * 5) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 5) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window1 top right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top middle
                if (zoom * (TILESIZE * 4) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 4) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window1 top middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top left
                if (zoom * (TILESIZE * 4) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 4) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window1 top left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                break;
        case WINDOW2:
                // bottom right
                if (zoom * (TILESIZE * 9) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 9) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window2 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom middle
                if (zoom * (TILESIZE * 9) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 9) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window2 bottom middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom left
                if (zoom * (TILESIZE * 8) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 8) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window2 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top right
                if (zoom * (TILESIZE * 9) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 9) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window2 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top middle
                if (zoom * (TILESIZE * 9) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 9) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window2 top middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top left
                if (zoom * (TILESIZE * 8) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 8) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window2 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                break;
        case WINDOW3:
                // bottom right
                if (zoom * (TILESIZE * 13) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 13) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window3 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom middle
                if (zoom * (TILESIZE * 13) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 13) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window3 bottom middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom left
                if (zoom * (TILESIZE * 12) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 12) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window3 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top right
                if (zoom * (TILESIZE * 13) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 13) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window3 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top middle
                if (zoom * (TILESIZE * 13) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 13) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window3 top middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top left
                if (zoom * (TILESIZE * 12) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 12) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window3 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                break;
        case WINDOW4:
                // bottom right
                if (zoom * (TILESIZE * 17) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 17) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window4 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom middle
                if (zoom * (TILESIZE * 17) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 17) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window4 bottom middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // bottom left
                if (zoom * (TILESIZE * 16) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 16) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 5) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 5) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window4 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top right
                if (zoom * (TILESIZE * 17) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 17) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window4 bottom right collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top middle
                if (zoom * (TILESIZE * 16) + (TILESIZE / 2) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 16) + (TILESIZE / 2) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window4 top middle collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                // top left
                if (zoom * (TILESIZE * 16) >= second_x + second_left_boundry &&
                    zoom * (TILESIZE * 16) <= second_x + second_right_boundry && 
                    zoom * (TILESIZE * 4) >= second_y + second_top_boundry &&
                    zoom * (TILESIZE * 4) <= second_y + second_bottom_boundry) {
                        // collision!
                        //printf("Window4 bottom left collision detected!\n");
                        //printf("Second character number: %i\n", second_character_number);
                        //collision = TRUE;
                        // if second char is a sword, then do damage
                        if (second_character_number == SWORD) {
                                *first_hit = TRUE;
                        }
                        break;
                }
                break;
        // boundry
        case BOUNDRY:
                printf("checking boundries\n");
                if (*first_x > 190 * zoom) {       // screen is X: 0-200 Y: 20-120
                        *first_x = 190 * zoom;
                        printf("first hit east boundry!\n");
                        collision = TRUE;
                }
                if (*first_x < 0) {
                        *first_x = 0;
                        printf("first hit west boundry!\n");
                        collision = TRUE;
                }
                if (*first_y > 110 * zoom) {
                        *first_y = 110 * zoom;
                        printf("first hit south boundry!\n");
                        collision = TRUE;
                }
                if (*first_y < 20 * zoom) {
                        *first_y = 20 * zoom;
                        printf("first hit north boundry!\n");
                        collision = TRUE;
                }
                break;
        default:
                break;
        }
        if (collision) {
                //printf ("collision detected!\n");
                // undo movement based on velocity if not sword
                if (second_character_number != SWORD) {
                        if (*first_velocity_x == 1) {
                                *first_x -= zoom;
                        }
                        if (*first_velocity_x == -1) {
                                *first_x += zoom;
                        }
                        if (*first_velocity_y == 1) {
                                *first_y -= zoom;
                        }
                        if (*first_velocity_y == -1) {
                                *first_y += zoom;
                        }
                }
                // change direction on collision
                // if not ed
                if (first_character_number  != ED) {
                        turn_right(first_direction, first_velocity_x, first_velocity_y);
                        //turn_left(first_direction, first_velocity_x, first_velocity_y);
                }
                return 1;
        }
        return 0;
}
// end collide_move_first

// functions.h
#ifndef _SDL_h
#include <SDL.h>
#endif
#ifndef _definitions_h
#include "definitions.h"
#endif

#ifndef _functions_h
#define _functions_h

// our quick complain and quit function
void complainAndExit(void);

// getpixel function retrieves the data from a single pixel
Uint32 getpixel(SDL_Surface *surface, int x, int y);

// putpixel function writes one pixel to a surface
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);

// the grow function will double the size of an image
SDL_Surface* grow(SDL_Surface *surface, int zoom);

// get_prefs_text() will read our terrain.txt file into a char[]
int *get_prefs_text(int prefs_text[], struct game_settings LoP_settings);

// get_terrain_text() will read our terrain.txt file into a char[]
int *get_terrain_text(int tiles[]);

/* get_map will load just the map tile you want
 * for a larger array. */
int get_map(int sceneNumber, int terrainTXT[], int i);

// turn a character right
int turn_right(int *direction, int *velocity_x, int *velocity_y);

// turn a character left
int turn_left(int *direction, int *velocity_x, int *velocity_y);

// turn a character around
int turn_around(int *direction, int *velocity_x, int *velocity_y);

// wobble_frame function changes the keyframe in a wobble pattern: 0->1->0->2
int wobble_frame(int step, int frameSpeed);

// loop_frame function changes the keyframe in a rotating pattern: 0->1->2->0
int loop_frame(int step, int frameSpeed);

// return the x and y coordinates for the tile's representative character
int get_tile_coords(char scene_char, short int *x, short int *y, short int *block, int zoom);

// if collision deteced return TRUE
int detect_collision(short int first_character_number, 
                int *first_x, 
                int *first_y, 
                int *first_velocity_x,
                int *first_velocity_y,
                int *first_direction,
                int *first_bump_direction,
                int *first_hit,
                short int second_character_number,
                int second_x, 
                int second_y,
                int second_direction,
                int zoom);

#endif

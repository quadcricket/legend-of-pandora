// definitions.h
#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

// feels weird having to define TRUE and FALSE ;)
#define TRUE 1
#define FALSE 0

// feels weird having to creal a boolean variable type
typedef unsigned char bool;

// how many miliseconds before ed moves a pixel
#define CHARSPEED 16

// how many miliseconds the attack lasts
#define ATTACK_DELAY 150


// how long does it take to climb a dungeon ladder
//  MUST BE CLEANLY DIVISIBLE BY 3!
#define LADDER_CLIMB_TIME 999

// defining a number for direction
#define NORTH 1
#define SOUTH 2
#define WEST  3
#define EAST  4
#define NO_DIRECTION 0

// ed definitions
#define ED 0            // used by collision detection
#define EDSPEED 8
#define ED_HEALTH 12
// this is how many milisecond pass before the keyframe
// is changed for animation (25 in windows, 200 in linux)
#ifdef __POCC__
#define EDFRAMESPEED 25
#endif
#ifdef __linux__
#define EDFRAMESPEED 200
#endif
#define ED_INVINCIBILITY_TIME 1000
// VanFire #define ED_START_SCENE 15
// King Troll #define ED_START_SCENE 0
// Tempus #define ED_START_SCENE 25
// Default #define ED_START_SCENE 28
#define ED_START_SCENE 28
#define ED_START_X 30
#define ED_START_Y 90

// monster definitions
#define MAXMONSTER 6	// how many monsters can spawn
#define MONSTER_INVINCIBILITY_TIME 100

#define RAT 1
#define RATSPEED 32
#define RAT_HEALTH 2
#define RATFRAMESPEED 400

#define TROLL 2
#define TROLLSPEED 30	// how many miliseconds before the monster moves a pixel (32)
#define TROLL_HEALTH 3
#define TROLLFRAMESPEED 400	// must be cleanly divisible by 4 if using the wobble_frame 
				//(how many miliseconds each frame lasts)
#define BAT 3
#define BATSPEED 16             // how many miliseconds before the monster moves a pixel (16) (higher is slower)
#define BAT_HEALTH 2
#define BATFRAMESPEED 252	// must be cleanly divisible by 3 if using the loop_frame

#define GHOST 4
#define GHOSTSPEED 24 
#define GHOST_HEALTH 2
#define GHOSTFRAMESPEED 1600

#define ROCK 5
#define ROCKSPEED 24
#define ROCKFRAMESPEED 800

#define FLAME 6

#define BALL 7

#define WALL 8

#define SWORD 9

#define BOSS_INVINCIBILITY_TIME 200
#define SCREEN_SHAKE_DURATION 1000

#define KING_TROLL 10
#define KINGTROLLSPEED 42 
#define KINGTROLLFRAMESPEED 1000

#define VANFIRE 11
#define VANFIRESPEED 42 
#define VANFIREFRAMESPEED 1000

#define TEMPUS 12
#define TEMPUSSPEED 42 
#define TEMPUSFRAMESPEED 1000

#define CRAIG 13

#define WINDOW1 14
#define WINDOW2 15
#define WINDOW3 16
#define WINDOW4 17

// the boundry of the screen (to keep monsters from leaving)
#define BOUNDRY 18

// shared definitions
//#define BUMP_TIME 1128
#define BUMP_TIME 128   // How many milliseconds should pass before the character stops
			// getting pushed back.

#define BUMP_SPEED 4	// How many pixels * delta the character will get pushed back

#define TILESIZE 10     // How big an unzoomed tile is (10x10)

#define NUMBER_OFFSET 112        // How far over the numbers are in the alphabet.bmp file
#define HEADER_SCORE_OFFSET_X 4 // how far over the score will be printed
#define HEADER_SCORE_OFFSET_Y 6 // how far down the score will be printed
#define HEADER_HEALTH_OFFSET_X 164       // how far over the health will start to be drawn
#define HEADER_HEALTH_OFFSET_Y 6        // how far down the health will start to be drawn

// game settings structure
struct game_settings {
        int width;
        int height;
        int zoom;
        bool full;
        int tilesize;
        bool mute;
        bool track_time;
};

#endif
